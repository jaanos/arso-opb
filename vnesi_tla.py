import csv
import psycopg2

print("Connecting...")

# povežem se z bazo

from auth import AUTH
try:
    conn = psycopg2.connect(**AUTH)
except psycopg2.OperationalError as e:
    print("\nI am unable to connect to the database with ATUH dict:", AUTH)
    print("Error:", e)

cursor = conn.cursor()

print("Done!")

print("Naredim slovar ime postaje : id postaje")

cursor.execute("SELECT id, ime FROM postaje")

ime_to_id = {ime: str(uid) for uid, ime in cursor.fetchall()}
# print(ime_to_id)

print("Done!")

print("Berem meritve ...")

cursor.execute("SELECT * FROM meritve;")
meritve = cursor.fetchall()
# print(type(meritve)) = LIST (seznam tuplov, kjer so vrednosti celic v vrstici)
# print(meritve)

katera_meritev = {(str(x[1]), str(x[2])): str(x[0]) for x in meritve}
##for i in katera_meritev:
##    print(i, katera_meritev[i])
##    print(type(i[0]), type(i[1])) # tipa sta int in datetime.date !!
##    break
# cursor.execute("SELECT * FROM meritve WHERE postajaId = 1322;")
# print(cursor.fetchall())
    
print("Done!")

def string_or_null(niz):
    return str(niz) if niz.strip() != '' else "\\N" # to je NULL
    

print("Berem podatke o temperaturah tal ...")

from opis_podatkov import convert_ime_postaje
from io import StringIO
import os
import datetime

missing = set()
path = 'raw_data/temperatura_tal'
filenames = os.listdir(path)
num_of_files = len(filenames)
c = 0
execute_meritve = []
execute_tla = []
for filename in filenames:
    print("\r {} / {}      {:.1f}%".format(c, num_of_files, c/num_of_files*100), end=' ')
    with open(os.path.join(path, filename)) as f:
        podatki = csv.DictReader(f, dialect='excel-tab')
        # ker so celice ločprint("\r {} / {}      {:.1f}%".format(c, num_of_files, c/num_of_files*100))ene s tab in ne z ,
        ime_postaje = convert_ime_postaje(filename[:-4].replace('_', ' '))
        IdPostaje = ime_to_id[ime_postaje]
        for row in podatki:
            #print(row)
            try:
                leto = int(row['leto'])
                mesec = int(row['mesec'])
                dan = int(row['dan'])
                datum = str(datetime.date(leto, mesec, dan))
                t2 = string_or_null(row['Ttla2cm'])
                t5 = string_or_null(row['Ttla5cm'])
                t10 = string_or_null(row['Ttla10cm'])
                t20 = string_or_null(row['Ttla20cm'])
                t30 = string_or_null(row['Ttla30cm'])
                t50 = string_or_null(row['Ttla50cm'])
                t100 = string_or_null(row['Ttla100cm'])
                t = [t2, t5, t10, t20, t30, t50, t100]
                visine = [2, 5, 10, 20, 30, 50, 100]
                execute_meritve.append((IdPostaje, datum))
                for i in range(len(visine)):
                    execute_tla.append(((IdPostaje, datum), str(visine[i]), str(t[i])))
                    # Zakaj morajo biti prav vse stringi, tudi če so v resnici v bazi FLOAT?
            except KeyError as e:
                missing.add(ime_postaje)
                # print(e)
    c += 1
print("\r {} / {}      {:.1f}%".format(c, num_of_files, c/num_of_files*100)) # da je na koncu 100% in ne ena manj kot vse

execute_tla = set(execute_tla)
execute_meritve = set(execute_meritve)
execute_meritve -= set(katera_meritev.keys()) # damo stran vse ki so ze v bazi

# trick for speed
from io import StringIO

if execute_meritve: # ce jih kaj se ostane
    f = StringIO('\n'.join(['\t'.join(x) for x in execute_meritve]))
    print("Missing stations:", missing)

    print("Shranjujem nove meritve v bazo ...")
    cursor.copy_from(f, 'meritve', columns=('postajaId', 'datum'))
    conn.commit()
    print("Done!")
else:
    print("Nothing to be done.")

print("Osvezujem meritve ...")

cursor.execute("SELECT * FROM meritve;")
meritve = cursor.fetchall()
katera_meritev = {(str(x[1]), str(x[2])): str(x[0]) for x in meritve}

print("Pripravljam podatke ...")
# zamenjaj datum in postajo v id meritve
execute_tla = [(katera_meritev[x[0]],) + x[1:] for x in execute_tla]

print("Dobivam obstojece podatke iz baze ...")
cols = ("id", "meritevId", "visina", "temperatura",)

cursor.execute("SELECT * FROM tla")
conv = lambda x: '\\N' if x is None or x == '\\N' else float(x)
stari_tla = set(tuple(map(conv, x[1:])) for x in cursor.fetchall())

execute_tla = {tuple(conv(x) for x in i) for i in execute_tla} # da se vse odšteje
execute_tla -= stari_tla
execute_tla = [tuple(str(y).rstrip('0').rstrip('.') for y in x) for x in execute_tla]

if execute_tla:
    print("Shranjujem nove podatke v bazo ({} novih) ...".format(len(execute_tla)))
    f = StringIO('\n'.join(['\t'.join(x) for x in execute_tla]))
    cursor.copy_from(f, 'tla', columns=cols[1:])
    conn.commit()
    print("Done.")
else:
    print("Nothing to be done.")


##print("Shranjujem temperature tal v bazo ...")
###  cursor.executemany("INSERT INTO meritve (postajaId, datum) VALUES (%s, %s);", execute)
##cursor.copy_from(f, 'tla', columns=('meritevId', 'visina', 'temperatura'))
##conn.commit()
##
##print("Done!")


conn.commit()
cursor.close()
conn.close()
print("\r {} / {}      {:.1f}%".format(c, num_of_files, c/num_of_files*100))
