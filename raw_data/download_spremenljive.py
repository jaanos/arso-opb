import urllib.request
import threading
import os.path

"""
Skripta zdownloada in shrani vse datoteke iz seznama urls.list. Da gre hitreje naredi
več niti, preveri, ali datoteke že obstajajo in si zapomni, če kakšne ne obstajajo (404s.list).
"""

with open("urls_spremenljivke.list") as f:
    urls = f.readlines()
urls = [x.strip() for x in urls]

NUMBER_OF_THREADS = 200
URLS_PER_THREAD = int(len(urls) / NUMBER_OF_THREADS)
url_list = []
for i in range(0, len(urls), URLS_PER_THREAD):
    url_list.append(urls[i : i + URLS_PER_THREAD])

err_fn = '404s_spremenljivke.list'
try:
    with open(err_fn) as f:
        e404s = set(x.strip() for x in f)
except FileNotFoundError:
    e404s = set()

c = 0
def download_worker(arr):
    global c
    for url in arr:
        fn = 'txt/' + url.split('/')[-1]
        if not os.path.isfile(fn) and fn not in e404s:
            try:
                pass
                w = urllib.request.urlopen(url)
                with open(fn, 'wb') as g:
                    g.write(w.read())
                    print("Saved ", fn)
            except urllib.error.HTTPError:
                e404s.add(fn)
        c += 1
        print("\r{} / {} ({:.2f} %)".format(c, len(urls), c / len(urls) * 100), end='')

try:
    threads = [threading.Thread(target=download_worker(l)) for l in url_list]
    for t in threads: t.start()
    for t in threads: t.join()
finally:
    with open(err_fn, 'w') as f:
        f.write('\n'.join(e404s))
print()
