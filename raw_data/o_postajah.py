import urllib.request
import os

"""
Skripta zdownloada in shrani podatke o postajah z Wikipedie.
"""

with open("postaje.txt", encoding = 'utf-8') as f:
    postaje = f.readlines()
postaje = [x.strip().split(',') for x in postaje]

# imena postaj moram preoblikovati v obliko, ki jo razume Wikipedia

postaje = [i[1] for i in postaje]
# print(postaje)

urls = [] # seznam ustreznih url naslovov, notri so tupli (ime z meteo, ime na wikipedii)
letalisca = [] # letališča imajo drugačno spletno stran, zato jih hranim posebej
osnova = r"http://sl.wikipedia.org/wiki/"

# letališča so problematična, zapišem si slovar
letalo = {'PORTOROŽ - LETALIŠČE' : 'Aerodrom_Portorož',
       'CERKLJE - LETALIŠČE' : 'Letališče_Cerklje_ob_Krki',
       'MARIBOR - LETALIŠČE' : 'Letališče_Edvarda_Rusjana_Maribor',
       'BRNIK - LETALIŠČE' : 'Letališče_Jožeta_Pučnika_Ljubljana'}

# funkcija, ki da ustrezne besede iz velikih tiskanih črk AAAA v Aaaa
# wikopedia je namreč občutljiva za velike začetnice
def male(bes):
    if bes.lower() in ['pri', 'na', 'pod', 'ob']:
        return bes.lower()
    return bes[0].upper() + bes[1:].lower()

for ime in postaje:
    if '-' in ime:
        if "LETALIŠČE" not in ime:
            # vzamem samo ime glavnega kraja, to je del pred znakom '-'
            besede = ime.split('-')[0].strip().split()
            ime_wiki = '_'.join(map(male, besede))
            urls.append((ime, osnova + ime_wiki))
        else:
            # letališča
            ime_wiki = letalo[ime]
            letalisca.append((ime, osnova + ime_wiki))
    else:
        # nekateri kraj povzročajo težave, zato jih obravnavam posebej
        if 'JERUZALEM' in ime:
            ime_wiki = 'Jeruzalem,_Ljutomer'
        elif 'BLOKAH' in ime:
            ime_wiki = 'Nova_vas,_Bloke'
        elif 'DOLENCI' in ime:
            ime_wiki = 'Dolenci,_Šalovci'
        elif 'BELI' in ime:
            # Beli križ je v Portorožu in nima svoje strani na wikipedii
            ime_wiki = 'Portorož'
        elif 'BILJE' in ime:
            ime_wiki = 'Bilje,_Miren_-_Kostanjevica'
        elif 'VOJSKO' in ime:
            ime_wiki = 'Vojsko,_Idrija'
        elif 'LISCA' in ime:
            ime_wiki = 'Lisca_(Posavje)'
        elif 'SEVNO' in ime:
            ime_wiki = 'Sevno,_Šmartno_pri_Litiji'
        else:
            besede = ime.strip().split()
            ime_wiki = '_'.join(map(male, besede))
        urls.append((ime, osnova + ime_wiki))


def make_valid_url(url):
    """ Spremeni unicode url v veljaven url s %XX encodingom. """
    url = urllib.parse.urlsplit(url)
    url = list(url)
    url[2] = urllib.parse.quote(url[2])
    return urllib.parse.urlunsplit(url)

# print(len(urls))
# print(urls)

def ime_dat(url):
    """Funkcija danemu url naslovu priredi ime datoteke (___.txt, brez šumnikov)."""
    return (url.split('/')[-1] + ".txt").replace('š', 's').replace('Š', 'S').replace('č', 'c').replace('Č', 'C').replace('ž', 'z').replace('Ž', 'Z')

def download_worker(arr, kam):
    """Funkcija potegne z interneta ustrezne strani. Izpiše problematične kraje."""
    for urlji in arr:
        ime_datoteke = urlji[0]
        url = urlji[1]
        ime = kam + '/' + ime_dat(ime_datoteke)
        if not os.path.isfile(ime):
                url = make_valid_url(url)
                # print (url)
                try:
                    html = urllib.request.urlopen(url)
                    with open(ime, 'wb') as g:
                        g.write(html.read())
                        print("Saved ", ime)
                except urllib.error.HTTPError as e:
                    print('NAPAKA!!!')
                    print(e)

# dejanski download
if not os.path.exists('postaje/'):
    os.mkdir('postaje/')
    print('Naredil sem novo mapo')
download_worker(urls, 'postaje')
print('Done postaje')
if not os.path.exists('letalisca/'):
    os.mkdir('letalisca/')
    print('Naredil sem novo mapo')
download_worker(letalisca, 'letalisca')
print('Done letalisca')
