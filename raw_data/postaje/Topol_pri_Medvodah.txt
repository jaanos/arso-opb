<!DOCTYPE html>
<html lang="sl" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>Topol pri Medvodah - Wikipedija, prosta enciklopedija</title>
<meta name="generator" content="MediaWiki 1.25wmf22" />
<link rel="alternate" href="android-app://org.wikipedia/http/sl.m.wikipedia.org/wiki/Topol_pri_Medvodah" />
<link rel="alternate" type="application/x-wiki" title="Uredi stran" href="/w/index.php?title=Topol_pri_Medvodah&amp;action=edit" />
<link rel="edit" title="Uredi stran" href="/w/index.php?title=Topol_pri_Medvodah&amp;action=edit" />
<link rel="apple-touch-icon" href="//bits.wikimedia.org/apple-touch/wikipedia.png" />
<link rel="shortcut icon" href="//bits.wikimedia.org/favicon/wikipedia.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="Wikipedija (sl)" />
<link rel="EditURI" type="application/rsd+xml" href="//sl.wikipedia.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/Topol_pri_Medvodah" />
<link rel="copyright" href="//creativecommons.org/licenses/by-sa/3.0/" />
<link rel="alternate" type="application/atom+xml" title="Atom-vir strani »Wikipedija«" href="/w/index.php?title=Posebno:ZadnjeSpremembe&amp;feed=atom" />
<link rel="canonical" href="http://sl.wikipedia.org/wiki/Topol_pri_Medvodah" />
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Topol_pri_Medvodah","wgTitle":"Topol pri Medvodah","wgCurRevisionId":4179705,"wgRevisionId":4179705,"wgArticleId":127879,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Naselja Občine Medvode","Izleti v okolici Ljubljane"],"wgBreakFrames":false,"wgPageContentLanguage":"sl","wgPageContentModel":"wikitext","wgSeparatorTransformTable":[",\t.",".\t,"],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy full","wgMonthNames":["","januar","februar","marec","april","maj","junij","julij","avgust","september","oktober","november","december"],"wgMonthNamesShort":["","jan.","feb.","mar.","apr.","maj","jun.","jul.","avg.","sep.","okt.","nov.","dec."],"wgRelevantPageName":"Topol_pri_Medvodah","wgRelevantArticleId":127879,"wgIsProbablyEditable":true,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgWikiEditorEnabledModules":{"toolbar":true,"dialogs":true,"hidesig":true,"preview":false,"publish":false},"wgMediaViewerOnClick":true,"wgMediaViewerEnabledByDefault":true,"wgVisualEditor":{"pageLanguageCode":"sl","pageLanguageDir":"ltr"},"wgPoweredByHHVM":true,"wgULSAcceptLanguageList":[],"wgULSCurrentAutonym":"slovenščina","wgBetaFeaturesFeatures":[],"wgCategoryTreePageCategoryOptions":"{\"mode\":0,\"hideprefix\":20,\"showcount\":true,\"namespaces\":false}","wgNoticeProject":"wikipedia","wgWikibaseItemId":"Q1962081","wgSiteNoticeId":"2.0"}); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"variant":"sl"});},{},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{},{});
/* cache key: slwiki:resourceloader:filter:minify-js:7:8cfd4e3db0e866eea1792f07f45e244b */ } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.load(["mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax","ext.centralauth.centralautologin","mmv.head","ext.imageMetrics.head","ext.visualEditor.viewPageTarget.init","ext.uls.init","ext.uls.interface","ext.centralNotice.bannerController","skins.vector.js","ext.dismissableSiteNotice"]); } );</script>
<link rel="stylesheet" href="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=ext.uls.nojs%7Cext.visualEditor.viewPageTarget.noscript%7Cext.wikihiero%2CwikimediaBadges%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.sectionAnchor%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.vector.styles%7Cwikibase.client.init&amp;only=styles&amp;skin=vector&amp;*" />
<meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=site&amp;only=styles&amp;skin=vector&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: slwiki:resourceloader:filter:minify-css:7:c6f8341d5f258571daa89c807f484bf1 */</style>
<script src="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=startup&amp;only=scripts&amp;skin=vector&amp;*"></script>
<link rel="dns-prefetch" href="//meta.wikimedia.org" />
<!--[if lt IE 7]><style type="text/css">body{behavior:url("/w/static-1.25wmf22/skins/Vector/csshover.min.htc")}</style><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-Topol_pri_Medvodah skin-vector action-view">
		<div id="mw-page-base" class="noprint"></div>
		<div id="mw-head-base" class="noprint"></div>
		<div id="content" class="mw-body" role="main">
			<a id="top"></a>

							<div id="siteNotice"><!-- CentralNotice --><script>document.write("\u003Cdiv class=\"mw-dismissable-notice\"\u003E\u003Cdiv class=\"mw-dismissable-notice-close\"\u003E[\u003Ca href=\"#\"\u003Eskrij\u003C/a\u003E]\u003C/div\u003E\u003Cdiv class=\"mw-dismissable-notice-body\"\u003E\u003Cdiv id=\"localNotice\" lang=\"sl\" dir=\"ltr\"\u003E\u003Ctable class=\"plainlinks\" style=\"margin: 4px 10%; border-collapse: collapse; background: #f9f9f9; border: 1px solid #f4c430;\"\u003E\n\u003Ctr\u003E\n\u003Ctd style=\"border: none; padding: 2px 0px 2px 0.9em; text-align: center;\"\u003E\n  \u003Cdiv class=\"floatright\"\u003E\u003Ca href=\"/wiki/Slika:CEE_Spring_banner-600.xcf\" class=\"image\"\u003E\u003Cimg alt=\"CEE Spring banner-600.xcf\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/120px-CEE_Spring_banner-600.xcf.png\" width=\"120\" height=\"32\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/180px-CEE_Spring_banner-600.xcf.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/240px-CEE_Spring_banner-600.xcf.png 2x\" data-file-width=\"600\" data-file-height=\"160\" /\u003E\u003C/a\u003E\u003C/div\u003E\u003C/td\u003E\n\u003Ctd style=\"border: none; padding: 0.25em 0.9em; width: 100%;\"\u003E V teku je spomladanska akcija pisanja člankov o \u003Ca href=\"/wiki/Srednja_Evropa\" title=\"Srednja Evropa\"\u003Esrednje-\u003C/a\u003E in \u003Ca href=\"/wiki/Vzhodna_Evropa\" title=\"Vzhodna Evropa\"\u003Evzhoevropskih\u003C/a\u003E državah - \u003Cb\u003E\u003Ca href=\"/wiki/Wikipedija:Wikimedia_CEE_Pomlad_2015\" title=\"Wikipedija:Wikimedia CEE Pomlad 2015\"\u003EWikimedia CEE Pomlad 2015\u003C/a\u003E\u003C/b\u003E. Vabljeni k vpisu in sodelovanju!\u003Cbr/\u003E\nDržavi za ta teden: \u003Ca href=\"/wiki/Slika:Flag_of_Azerbaijan.svg\" class=\"image\" title=\"Zastava Azerbajdžana\"\u003E\u003Cimg alt=\"Zastava Azerbajdžana\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/22px-Flag_of_Azerbaijan.svg.png\" width=\"22\" height=\"11\" class=\"thumbborder\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/33px-Flag_of_Azerbaijan.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/44px-Flag_of_Azerbaijan.svg.png 2x\" data-file-width=\"1200\" data-file-height=\"600\" /\u003E\u003C/a\u003E\u0026#160;\u003Ca href=\"/wiki/Azerbajd%C5%BEan\" title=\"Azerbajdžan\"\u003EAzerbajdžan\u003C/a\u003E (\u003Ca href=\"//meta.wikimedia.org/wiki/Wikimedia_CEE_Spring_2015/Structure/Azerbaijan\" class=\"extiw\" title=\"meta:Wikimedia CEE Spring 2015/Structure/Azerbaijan\"\u003Eideje\u003C/a\u003E) in \u003Ca href=\"/wiki/Slika:Flag_of_Belarus.svg\" class=\"image\" title=\"Zastava Belorusije\"\u003E\u003Cimg alt=\"Zastava Belorusije\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/22px-Flag_of_Belarus.svg.png\" width=\"22\" height=\"11\" class=\"thumbborder\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/33px-Flag_of_Belarus.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/44px-Flag_of_Belarus.svg.png 2x\" data-file-width=\"900\" data-file-height=\"450\" /\u003E\u003C/a\u003E\u0026#160;\u003Ca href=\"/wiki/Belorusija\" title=\"Belorusija\"\u003EBelorusija\u003C/a\u003E (\u003Ca href=\"//meta.wikimedia.org/wiki/Wikimedia_CEE_Spring_2015/Structure/Belarus\" class=\"extiw\" title=\"meta:Wikimedia CEE Spring 2015/Structure/Belarus\"\u003Eideje\u003C/a\u003E) \u003C/td\u003E\n\n\u003C/tr\u003E\n\u003C/table\u003E\n\u003C/div\u003E\u003C/div\u003E\u003C/div\u003E");</script></div>
						<div class="mw-indicators">
</div>
			<h1 id="firstHeading" class="firstHeading" lang="sl">Topol pri Medvodah</h1>
						<div id="bodyContent" class="mw-body-content">
									<div id="siteSub">Iz Wikipedije, proste enciklopedije</div>
								<div id="contentSub"></div>
												<div id="jump-to-nav" class="mw-jump">
					Skoči na:					<a href="#mw-head">navigacija</a>, 					<a href="#p-search">iskanje</a>
				</div>
				<div id="mw-content-text" lang="sl" dir="ltr" class="mw-content-ltr"><table class="infobox geography vcard" style="width:23em; text-align:left">
<tr>
<th colspan="2" style="width:100%; text-align:center; background-color:#B0C4DE; font-size:1.25em; white-space:nowrap"><span class="fn org">Topol pri Medvodah</span></th>
</tr>
<tr>
<td colspan="2" style="text-align:center; padding:0.7em 0.8em">
<div class="floatnone"><a href="/wiki/Slika:Topol_pri_Medvodah_Slovenia.JPG" class="image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/09/Topol_pri_Medvodah_Slovenia.JPG/260px-Topol_pri_Medvodah_Slovenia.JPG" width="260" height="150" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/0/09/Topol_pri_Medvodah_Slovenia.JPG/390px-Topol_pri_Medvodah_Slovenia.JPG 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/0/09/Topol_pri_Medvodah_Slovenia.JPG/520px-Topol_pri_Medvodah_Slovenia.JPG 2x" data-file-width="1605" data-file-height="929" /></a></div>
</td>
</tr>
<tr class="mergedrow">
<td colspan="2" style="text-align:center" align="center">
<center>
<div style="width:260px;float:none;clear:none">
<div style="width:260px;padding:0">
<div style="position:relative;width:260px"><a href="/wiki/Slika:Slovenia_location_map.svg" class="image" title="Topol pri Medvodah is located in Slovenija"><img alt="Topol pri Medvodah is located in Slovenija" src="//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Slovenia_location_map.svg/260px-Slovenia_location_map.svg.png" width="260" height="192" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Slovenia_location_map.svg/390px-Slovenia_location_map.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/e/e5/Slovenia_location_map.svg/520px-Slovenia_location_map.svg.png 2x" data-file-width="1159" data-file-height="857" /></a>
<div style="position:absolute;top:53%;left:32.7%;height:0;width:0;margin:0;padding:0">
<div style="position:relative;text-align:center;left:-3px;top:-3px;width:6px;font-size:6px;line-height:0"><img alt="Topol pri Medvodah" src="//upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Red_pog.svg/6px-Red_pog.svg.png" title="Topol pri Medvodah" width="6" height="6" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Red_pog.svg/9px-Red_pog.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/0/0c/Red_pog.svg/12px-Red_pog.svg.png 2x" data-file-width="64" data-file-height="64" /></div>
<div style="font-size:90%;line-height:110%;position:relative;top:-1.5em;width:6em;left:0.5em;text-align:left"><span style="padding:1px">Topol pri Medvodah</span></div>
</div>
</div>
<div style="font-size:90%;padding-top:3px"></div>
</div>
</div>
<small>Geografska lega v Sloveniji</small></center>
</td>
</tr>
<tr class="mergedbottomrow">
<td colspan="2" style="text-align:center; padding-bottom:0.7em"><a href="/wiki/Geografski_koordinatni_sistem" title="Geografski koordinatni sistem">Koordinati</a>: <span style="white-space: nowrap;"><span class="plainlinks nourlexpansion"><a class="external text" href="https://tools.wmflabs.org/geohack/geohack.php?pagename=Topol_pri_Medvodah&amp;params=46_5_37.59_N_14_22_38.25_E_region:SI_type:adm1st"><span class="geo-default"><span class="geo-dms" title="Zemljevidi, zračni posnetki in drugi podatki za to lokacijo"><span class="latitude">46°5′37.59″N</span> <span class="longitude">14°22′38.25″E</span></span></span><span class="geo-multi-punct">﻿ / ﻿</span><span class="geo-nondefault"><span class="geo-dec" title="Zemljevidi, zračni posnetki in drugi podatki za to lokacijo">46.093775°N 14.3772917°E</span><span style="display:none">﻿ / <span class="geo">46.093775; 14.3772917</span></span></span></a></span><span style="font-size: small;"><span id="coordinates"><a href="/wiki/Geografski_koordinatni_sistem" title="Geografski koordinatni sistem">Koordinati</a>: <span class="plainlinks nourlexpansion"><a class="external text" href="https://tools.wmflabs.org/geohack/geohack.php?pagename=Topol_pri_Medvodah&amp;params=46_5_37.59_N_14_22_38.25_E_region:SI_type:adm1st"><span class="geo-default"><span class="geo-dms" title="Zemljevidi, zračni posnetki in drugi podatki za to lokacijo"><span class="latitude">46°5′37.59″N</span> <span class="longitude">14°22′38.25″E</span></span></span><span class="geo-multi-punct">﻿ / ﻿</span><span class="geo-nondefault"><span class="geo-dec" title="Zemljevidi, zračni posnetki in drugi podatki za to lokacijo">46.093775°N 14.3772917°E</span><span style="display:none">﻿ / <span class="geo">46.093775; 14.3772917</span></span></span></a></span></span></span></span></td>
</tr>
<tr class="mergedtoprow">
<td><b><a href="/wiki/Seznam_suverenih_dr%C5%BEav" title="Seznam suverenih držav">Država</a></b></td>
<td><a href="/wiki/Slika:Flag_of_Slovenia.svg" class="image" title="Zastava Slovenije"><img alt="Zastava Slovenije" src="//upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Flag_of_Slovenia.svg/22px-Flag_of_Slovenia.svg.png" width="22" height="11" class="thumbborder" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Flag_of_Slovenia.svg/33px-Flag_of_Slovenia.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Flag_of_Slovenia.svg/44px-Flag_of_Slovenia.svg.png 2x" data-file-width="1200" data-file-height="600" /></a> <a href="/wiki/Slovenija" title="Slovenija">Slovenija</a></td>
</tr>
<tr class="mergedrow">
<td><b><a href="/wiki/Statisti%C4%8Dne_regije_Slovenije" title="Statistične regije Slovenije">Statistična regija</a></b></td>
<td><a href="/wiki/Gorenjska_regija" title="Gorenjska regija">Gorenjska regija</a></td>
</tr>
<tr class="mergedrow">
<td><b><a href="/wiki/Pokrajine_v_Sloveniji" title="Pokrajine v Sloveniji">Tradicionalna pokrajina</a></b></td>
<td><a href="/wiki/Gorenjska" title="Gorenjska">Gorenjska</a></td>
</tr>
<tr class="mergedrow">
<td><b><a href="/wiki/Ob%C4%8Dina" title="Občina">Občina</a></b></td>
<td><a href="/wiki/Ob%C4%8Dina_Medvode" title="Občina Medvode">Medvode</a></td>
</tr>
<tr class="mergedtoprow">
<td><b>Nadmorska višina</b></td>
<td>645,6&#160;m</td>
</tr>
<tr class="mergedtoprow">
<td colspan="2"><b>Prebivalstvo</b></td>
</tr>
<tr class="mergedrow">
<td>&#160;•&#160;<b>Skupno</b></td>
<td>151</td>
</tr>
<tr class="mergedtoprow">
<td><b><a href="/wiki/%C4%8Casovni_pas" title="Časovni pas">Časovni pas</a></b></td>
<td><a href="/wiki/Srednjeevropski_%C4%8Das" title="Srednjeevropski čas">CET</a> (<a href="/wiki/UTC%2B1" title="UTC+1">UTC+1</a>)</td>
</tr>
<tr class="mergedrow">
<td style="white-space:nowrap">&#160;•&#160;<b>Poletje&#160;(<a href="/wiki/Poletni_%C4%8Das" title="Poletni čas">DST</a>)</b></td>
<td><a href="/wiki/Srednjeevropski_poletni_%C4%8Das" title="Srednjeevropski poletni čas">CEST</a> (<a href="/wiki/UTC%2B2" title="UTC+2">UTC+2</a>)</td>
</tr>
<tr class="mergedtoprow">
<td><b><a href="/wiki/Seznam_po%C5%A1tnih_%C5%A1tevilk_v_Sloveniji" title="Seznam poštnih številk v Sloveniji">Poštna številka</a></b></td>
<td class="adr"><span class="postal-code">1215 <a href="/wiki/Medvode" title="Medvode">Medvode</a></span></td>
</tr>
<tr class="mergedtoprow">
<td><b>Zemljevidi</b></td>
<td><a rel="nofollow" class="external text" href="http://zemljevid.najdi.si/search_maps.jsp?q=Topol+pri+Medvodah&amp;tab=maps">Najdi.si</a>, <a rel="nofollow" class="external text" href="http://www.geopedia.si/#L410_T13_F10110904_b4">Geopedia.si</a></td>
</tr>
<tr class="mergedtoprow">
<td colspan="2" style="text-align:left; font-size:smaller"><small>Vir: <a href="/wiki/Statisti%C4%8Dni_urad_Republike_Slovenije" title="Statistični urad Republike Slovenije">SURS</a>, <a href="/wiki/Geodetska_uprava_Republike_Slovenije" title="Geodetska uprava Republike Slovenije">GURS</a>, <a href="/wiki/Popis_prebivalstva" title="Popis prebivalstva">popis prebivalstva</a> 2002 (kjer ni drugače navedeno).</small><br /></td>
</tr>
</table>
<p><b>Topol pri Medvodah</b>, pogosto kar <b>Katarina nad Ljubljano</b> je <a href="/wiki/Naselje" title="Naselje">naselje</a> v <a href="/wiki/Ob%C4%8Dina_Medvode" title="Občina Medvode">Občini Medvode</a>, ki meji na <a href="/wiki/Ob%C4%8Dina_Dobrova_-_Polhov_Gradec" title="Občina Dobrova - Polhov Gradec">občino Dobrova - Polhov Gradec</a>. Zaselek je prljubljena izletniška točka mnogim ljubljančanom, saj predstavlja izhodišče za sprehode na bližnje vzpetine <a href="/wiki/Polhograjsko_hribovje" title="Polhograjsko hribovje">Polhograjskega hribovja</a>, ki jih predstavljajo <a href="/w/index.php?title=Sv._Jakob_(Polhograjski_dolomiti)&amp;action=edit&amp;redlink=1" class="new" title="Sv. Jakob (Polhograjski dolomiti) (stran ne obstaja)">Sv. Jakob</a>, <a href="/w/index.php?title=Jeterbenk&amp;action=edit&amp;redlink=1" class="new" title="Jeterbenk (stran ne obstaja)">Jeterbenk</a>, <a href="/w/index.php?title=Grmada_(Polhograjski_dolomiti)&amp;action=edit&amp;redlink=1" class="new" title="Grmada (Polhograjski dolomiti) (stran ne obstaja)">Grmada</a> in <a href="/w/index.php?title=To%C5%A1%C4%8D&amp;action=edit&amp;redlink=1" class="new" title="Tošč (stran ne obstaja)">Tošč</a>. Preko teh vrhov vodijo številne pešpoti:</p>
<ul>
<li><a href="/wiki/Evropska_pe%C5%A1pot_E-7" title="Evropska pešpot E-7">Evropska pešpot E-7</a></li>
<li><a href="/w/index.php?title=Kurirska_pot_Dolomitov&amp;action=edit&amp;redlink=1" class="new" title="Kurirska pot Dolomitov (stran ne obstaja)">Kurirska pot Dolomitov</a></li>
<li><a href="/w/index.php?title=Ljubljanska_mladinska_pot&amp;action=edit&amp;redlink=1" class="new" title="Ljubljanska mladinska pot (stran ne obstaja)">Ljubljanska mladinska pot</a></li>
<li><a href="/w/index.php?title=Pohodna_transverzala_spominov_ob%C4%8Dine_Ljubljana-%C5%A0i%C5%A1ka&amp;action=edit&amp;redlink=1" class="new" title="Pohodna transverzala spominov občine Ljubljana-Šiška (stran ne obstaja)">Pohodna transverzala spominov občine Ljubljana-Šiška</a></li>
<li><a href="/w/index.php?title=Polhograjska_planinska_pot&amp;action=edit&amp;redlink=1" class="new" title="Polhograjska planinska pot (stran ne obstaja)">Polhograjska planinska pot</a></li>
<li><a href="/w/index.php?title=Slovenska_geolo%C5%A1ka_pot&amp;action=edit&amp;redlink=1" class="new" title="Slovenska geološka pot (stran ne obstaja)">Slovenska geološka pot</a></li>
<li><a href="/w/index.php?title=Transverzala_kurirjev_in_vezistov&amp;action=edit&amp;redlink=1" class="new" title="Transverzala kurirjev in vezistov (stran ne obstaja)">Transverzala kurirjev in vezistov</a></li>
<li><a href="/w/index.php?title=Transverzalna_pot_Ljubljana-Triglav&amp;action=edit&amp;redlink=1" class="new" title="Transverzalna pot Ljubljana-Triglav (stran ne obstaja)">Transverzalna pot Ljubljana-Triglav</a></li>
</ul>
<p>Do leta <a href="/wiki/1955" title="1955">1955</a> je bilo ime zaselka <b>Katarina nad Medvodami</b> (po <a href="/wiki/%C5%BDupnija_Sv._Katarina_-_Topol" title="Župnija Sv. Katarina - Topol">župnijski cerkvi</a>), odtlej pa Topol, vendar staro ime ostaja zakoreninjeno med okoliškimi prebivalci. Uradna vremenska napoved Hidrometeorološkega zavoda Slovenije posreduje podatke za <i>Katarino nad Ljubljano</i>.</p>
<p>Dostop do naselja je mogoč po treh cestah: iz <a href="/wiki/Medvode" title="Medvode">Medvod</a> (preko kraja <a href="/wiki/Sora,_Medvode" title="Sora, Medvode">Sora</a>), iz <a href="/wiki/Dobrova_pri_Ljubljani" title="Dobrova pri Ljubljani" class="mw-redirect">Dobrove</a> in iz <a href="/wiki/Ljubljana" title="Ljubljana">Ljubljane</a> (čez <a href="/wiki/To%C5%A1ko_%C4%8Celo" title="Toško Čelo">Toško Čelo</a>). Nekdaj je bil Topol povezan z Ljubljano z redno primestno avtobusno linijo.</p>
<h2><a href="#Glej_tudi" class="mw-headline-anchor" aria-hidden="true" title="Povezava na razdelek">§</a><span class="mw-headline" id="Glej_tudi">Glej tudi</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Topol_pri_Medvodah&amp;veaction=edit&amp;vesection=1" title="Spremeni razdelek: Glej tudi" class="mw-editsection-visualeditor">uredi</a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Topol_pri_Medvodah&amp;action=edit&amp;section=1" title="Spremeni razdelek: Glej tudi">uredi kodo</a><span class="mw-editsection-bracket">]</span></span></h2>
<table class="metadata mbox-small plainlinks" style="border:1px solid #aaa; background-color:#f9f9f9;">
<tr>
<td class="mbox-image"><img alt="" src="//upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/30px-Commons-logo.svg.png" width="30" height="40" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/45px-Commons-logo.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/59px-Commons-logo.svg.png 2x" data-file-width="1024" data-file-height="1376" /></td>
<td class="mbox-text plainlist" style="">Wikimedijina zbirka ponuja več predstavnostnega gradiva o temi: <i><b><a href="//commons.wikimedia.org/wiki/Category:Topol_pri_Medvodah" class="extiw" title="commons:Category:Topol pri Medvodah">Topol pri Medvodah</a></b></i></td>
</tr>
</table>
<ul>
<li><a href="/wiki/Seznam_naselij_v_Sloveniji" title="Seznam naselij v Sloveniji">seznam naselij v Sloveniji</a></li>
<li><a href="/w/index.php?title=Jama_Malinca&amp;action=edit&amp;redlink=1" class="new" title="Jama Malinca (stran ne obstaja)">jama Malinca</a></li>
</ul>


<!-- 
NewPP limit report
Parsed by mw1073
CPU time usage: 0.235 seconds
Real time usage: 0.319 seconds
Preprocessor visited node count: 1823/1000000
Preprocessor generated node count: 0/1500000
Post‐expand include size: 27399/2097152 bytes
Template argument size: 5216/2097152 bytes
Highest expansion depth: 22/40
Expensive parser function count: 1/500
Lua time usage: 0.011/10.000 seconds
Lua memory usage: 587 KB/50 MB
-->

<!-- 
Transclusion expansion time report (%,ms,calls,template)
100.00%  202.707      1 - -total
 87.41%  177.193      1 - Predloga:Infopolje_Naselje_v_Sloveniji
 82.62%  167.484      1 - Predloga:Infopolje_Naselje
 20.97%   42.499      1 - Predloga:Geobox_coor
 19.67%   39.877      1 - Predloga:Location_map+
 18.07%   36.628      1 - Predloga:Coord
 14.57%   29.532      1 - Predloga:Koord/prikaži/inline,title
 12.46%   25.266      1 - Predloga:Kategorija_v_Zbirki
 12.14%   24.612      1 - Predloga:Koord/vnos/dms
 11.43%   23.165      1 - Predloga:Zbirka
-->

<!-- Saved in parser cache with key slwiki:pcache:idhash:127879-0!*!0!*!*!4!* and timestamp 20150312122328 and revision id 4179705
 -->
<noscript><img src="//sl.wikipedia.org/wiki/Special:CentralAutoLogin/start?type=1x1" alt="" title="" width="1" height="1" style="border: none; position: absolute;" /></noscript></div>									<div class="printfooter">
						Vzpostavljeno iz&#160;»<a dir="ltr" href="http://sl.wikipedia.org/w/index.php?title=Topol_pri_Medvodah&amp;oldid=4179705">http://sl.wikipedia.org/w/index.php?title=Topol_pri_Medvodah&amp;oldid=4179705</a>«					</div>
													<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Posebno:Kategorije" title="Posebno:Kategorije">Kategoriji</a>: <ul><li><a href="/wiki/Kategorija:Naselja_Ob%C4%8Dine_Medvode" title="Kategorija:Naselja Občine Medvode">Naselja Občine Medvode</a></li><li><a href="/wiki/Kategorija:Izleti_v_okolici_Ljubljane" title="Kategorija:Izleti v okolici Ljubljane">Izleti v okolici Ljubljane</a></li></ul></div></div>												<div class="visualClear"></div>
							</div>
		</div>
		<div id="mw-navigation">
			<h2>Navigacijski meni</h2>

			<div id="mw-head">
									<div id="p-personal" role="navigation" class="" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Osebna orodja</h3>
						<ul>
							<li id="pt-createaccount"><a href="/w/index.php?title=Posebno:Prijava&amp;returnto=Topol+pri+Medvodah&amp;type=signup" title="Predlagamo vam, da ustvarite račun in se prijavite, vendar pa to ni obvezno.">Ustvari račun</a></li><li id="pt-login"><a href="/w/index.php?title=Posebno:Prijava&amp;returnto=Topol+pri+Medvodah" title="Prijava ni obvezna, vendar je zaželena [o]" accesskey="o">Prijava</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Imenski prostori</h3>
						<ul>
															<li  id="ca-nstab-main" class="selected"><span><a href="/wiki/Topol_pri_Medvodah"  title="Prikaže članek [c]" accesskey="c">Stran</a></span></li>
															<li  id="ca-talk" class="new"><span><a href="/w/index.php?title=Pogovor:Topol_pri_Medvodah&amp;action=edit&amp;redlink=1"  title="Pogovor o strani [t]" accesskey="t">Pogovor</a></span></li>
													</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<h3 id="p-variants-label"><span>Različice</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Pogled</h3>
						<ul>
															<li id="ca-view" class="selected"><span><a href="/wiki/Topol_pri_Medvodah" >Preberi</a></span></li>
															<li id="ca-ve-edit"><span><a href="/w/index.php?title=Topol_pri_Medvodah&amp;veaction=edit"  title="Urejanje strani z VisualEditorjem [v]" accesskey="v">Uredi</a></span></li>
															<li id="ca-edit" class=" collapsible"><span><a href="/w/index.php?title=Topol_pri_Medvodah&amp;action=edit"  title="Stran lahko uredite. Preden jo shranite, uporabite gumb za predogled. [e]" accesskey="e">Uredi kodo</a></span></li>
															<li id="ca-history" class="collapsible"><span><a href="/w/index.php?title=Topol_pri_Medvodah&amp;action=history"  title="Prejšnje redakcije strani [h]" accesskey="h">Zgodovina</a></span></li>
													</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<h3 id="p-cactions-label"><span>Več</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Iskanje</label>
						</h3>

						<form action="/w/index.php" id="searchform">
														<div id="simpleSearch">
															<input type="search" name="search" placeholder="Iskanje" title="Preiščite wiki [f]" accesskey="f" id="searchInput" /><input type="hidden" value="Posebno:Iskanje" name="title" /><input type="submit" name="fulltext" value="Iskanje" title="Najde vneseno besedilo po straneh" id="mw-searchButton" class="searchButton mw-fallbackSearchButton" /><input type="submit" name="go" value="Pojdi na" title="Pojdi na stran z natanko takim imenom, če obstaja" id="searchButton" class="searchButton" />								</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Glavna_stran"  title="Glavna stran"></a></div>
						<div class="portal" role="navigation" id='p-navigation' aria-labelledby='p-navigation-label'>
			<h3 id='p-navigation-label'>Navigacija</h3>

			<div class="body">
									<ul>
													<li id="n-mainpage"><a href="/wiki/Glavna_stran" title="Obiščite Glavno stran [z]" accesskey="z">Glavna stran</a></li>
													<li id="n-Dobrodo.C5.A1li"><a href="/wiki/Wikipedija:Uvod">Dobrodošli</a></li>
													<li id="n-Izbrani-.C4.8Dlanki"><a href="/wiki/Wikipedija:Izbrani_%C4%8Dlanki">Izbrani članki</a></li>
													<li id="n-randompage"><a href="/wiki/Posebno:Naklju%C4%8Dno" title="Naložite naključno stran [x]" accesskey="x">Naključni članek</a></li>
													<li id="n-recentchanges"><a href="/wiki/Posebno:ZadnjeSpremembe" title="Seznam zadnjih sprememb Wikipedije [r]" accesskey="r">Zadnje spremembe</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-obcestvo' aria-labelledby='p-obcestvo-label'>
			<h3 id='p-obcestvo-label'>Občestvo</h3>

			<div class="body">
									<ul>
													<li id="n-portal"><a href="/wiki/Wikipedija:Portal_ob%C4%8Destva" title="O projektu, kaj lahko storite, kje lahko kaj najdete">Portal občestva</a></li>
													<li id="n-Pod-lipo"><a href="/wiki/Wikipedija:Pod_lipo">Pod lipo</a></li>
													<li id="n-contact"><a href="/wiki/Wikipedija:Stik_z_nami">Kontaktna stran</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-podpora' aria-labelledby='p-podpora-label'>
			<h3 id='p-podpora-label'>Podpora</h3>

			<div class="body">
									<ul>
													<li id="n-help"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents" title="Kraj za pomoč">Pomoč</a></li>
													<li id="n-sitesupport"><a href="//donate.wikimedia.org/wiki/Special:FundraiserRedirector?utm_source=donate&amp;utm_medium=sidebar&amp;utm_campaign=C13_sl.wikipedia.org&amp;uselang=sl" title="Podprite nas">Denarni prispevki</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-coll-print_export' aria-labelledby='p-coll-print_export-label'>
			<h3 id='p-coll-print_export-label'>Tiskanje/izvoz</h3>

			<div class="body">
									<ul>
													<li id="coll-create_a_book"><a href="/w/index.php?title=Posebno:Book&amp;bookcmd=book_creator&amp;referer=Topol+pri+Medvodah">Ustvari e-knjigo</a></li>
													<li id="coll-download-as-rdf2latex"><a href="/w/index.php?title=Posebno:Book&amp;bookcmd=render_article&amp;arttitle=Topol+pri+Medvodah&amp;oldid=4179705&amp;writer=rdf2latex">Prenesi kot PDF</a></li>
													<li id="t-print"><a href="/w/index.php?title=Topol_pri_Medvodah&amp;printable=yes" title="Natisljiva različica strani [p]" accesskey="p">Različica za tisk</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-tb' aria-labelledby='p-tb-label'>
			<h3 id='p-tb-label'>Orodja</h3>

			<div class="body">
									<ul>
													<li id="t-whatlinkshere"><a href="/wiki/Posebno:KajSePovezujeSem/Topol_pri_Medvodah" title="Seznam vseh s trenutno povezanih strani [j]" accesskey="j">Kaj se povezuje sem</a></li>
													<li id="t-recentchangeslinked"><a href="/wiki/Posebno:RecentChangesLinked/Topol_pri_Medvodah" title="Zadnje spremembe na s trenutno povezanih straneh [k]" accesskey="k">Sorodne spremembe</a></li>
													<li id="t-specialpages"><a href="/wiki/Posebno:PosebneStrani" title="Preglejte seznam vseh posebnih strani [q]" accesskey="q">Posebne strani</a></li>
													<li id="t-permalink"><a href="/w/index.php?title=Topol_pri_Medvodah&amp;oldid=4179705" title="Stalna povezava na to različico strani">Trajna povezava</a></li>
													<li id="t-info"><a href="/w/index.php?title=Topol_pri_Medvodah&amp;action=info" title="Več informacij o strani">Podatki o strani</a></li>
													<li id="t-wikibase"><a href="//www.wikidata.org/wiki/Q1962081" title="Povežite na ustrezni predmet v podatkovnem odložišču [g]" accesskey="g">Objekt Wikipodatki</a></li>
						<li id="t-cite"><a href="/w/index.php?title=Posebno:Navedi&amp;page=Topol_pri_Medvodah&amp;id=4179705" title="Informacije o tem, kako navajati to stran">Navedba članka</a></li>					</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-lang' aria-labelledby='p-lang-label'>
			<h3 id='p-lang-label'>V drugih jezikih</h3>

			<div class="body">
									<ul>
													<li class="interlanguage-link interwiki-en"><a href="//en.wikipedia.org/wiki/Topol_pri_Medvodah" title="Topol pri Medvodah – angleščina" lang="en" hreflang="en">English</a></li>
													<li class="interlanguage-link interwiki-nl"><a href="//nl.wikipedia.org/wiki/Topol_pri_Medvodah" title="Topol pri Medvodah – nizozemščina" lang="nl" hreflang="nl">Nederlands</a></li>
													<li class="interlanguage-link interwiki-ro"><a href="//ro.wikipedia.org/wiki/Topol_pri_Medvodah" title="Topol pri Medvodah – romunščina" lang="ro" hreflang="ro">Română</a></li>
													<li class="interlanguage-link interwiki-uk"><a href="//uk.wikipedia.org/wiki/%D0%A2%D0%BE%D0%BF%D0%BE%D0%BB-%D0%BF%D1%80%D0%B8-%D0%9C%D0%B5%D0%B4%D0%B2%D0%BE%D0%B4%D0%B0%D1%85" title="Топол-при-Медводах – ukrajinščina" lang="uk" hreflang="uk">Українська</a></li>
													<li class="uls-p-lang-dummy"><a href="#"></a></li>
											</ul>
				<div class='after-portlet after-portlet-lang'><span class="wb-langlinks-edit wb-langlinks-link"><a href="//www.wikidata.org/wiki/Q1962081#sitelinks-wikipedia" title="Uredi medjezikovne povezave" class="wbc-editpage">Uredi povezave</a></span></div>			</div>
		</div>
				</div>
		</div>
		<div id="footer" role="contentinfo">
							<ul id="footer-info">
											<li id="footer-info-lastmod"> Čas zadnje spremembe: 18:30, 1. april 2014.</li>
											<li id="footer-info-copyright">Besedilo se sme prosto uporabljati v skladu z dovoljenjem <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons 
Priznanje avtorstva-Deljenje pod enakimi pogoji 3.0</a>; uveljavljajo se lahko dodatni pogoji. Za podrobnosti glej <a href="//wikimediafoundation.org/wiki/Terms_of_Use">Pogoje uporabe</a>.<br/>
Wikipedia® je tržna znamka neprofitne organizacije <a href="http://wikimediafoundation.org">Wikimedia Foundation Inc.</a></li>
									</ul>
							<ul id="footer-places">
											<li id="footer-places-privacy"><a href="//wikimediafoundation.org/wiki/Politika_zasebnosti" title="wikimedia:Politika zasebnosti">Politika zasebnosti</a></li>
											<li id="footer-places-about"><a href="/wiki/Wikipedija:O_Wikipediji" title="Wikipedija:O Wikipediji">O Wikipediji</a></li>
											<li id="footer-places-disclaimer"><a href="/wiki/Wikipedija:Splo%C5%A1no_zanikanje_odgovornosti" title="Wikipedija:Splošno zanikanje odgovornosti">Zanikanja odgovornosti</a></li>
											<li id="footer-places-developers"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/How_to_contribute">Razvijalci</a></li>
											<li id="footer-places-mobileview"><a href="//sl.m.wikipedia.org/w/index.php?title=Topol_pri_Medvodah&amp;mobileaction=toggle_view_mobile" class="noprint stopMobileRedirectToggle">Mobilni pogled</a></li>
									</ul>
										<ul id="footer-icons" class="noprint">
											<li id="footer-copyrightico">
															<a href="//wikimediafoundation.org/"><img src="//bits.wikimedia.org/images/wikimedia-button.png" srcset="//bits.wikimedia.org/images/wikimedia-button-1.5x.png 1.5x, //bits.wikimedia.org/images/wikimedia-button-2x.png 2x" width="88" height="31" alt="Wikimedia Foundation"/></a>
													</li>
											<li id="footer-poweredbyico">
															<a href="//www.mediawiki.org/"><img src="//bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="//bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_132x47.png 1.5x, //bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31" /></a>
													</li>
									</ul>
						<div style="clear:both"></div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.state({"ext.globalCssJs.site":"ready","ext.globalCssJs.user":"ready","site":"loading","user":"ready","user.groups":"ready"}); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.load(["mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest","ext.wikiEditor.init","mmv.bootstrap.autostart","ext.imageMetrics.loader","ext.visualEditor.targetLoader","ext.eventLogging.subscriber","ext.wikimediaEvents.statsd","ext.navigationTiming","schema.UniversalLanguageSelector","ext.uls.eventlogger","ext.uls.interlanguage"],null,true); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { document.write("\u003Cscript src=\"//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false\u0026amp;lang=sl\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=vector\u0026amp;*\"\u003E\u003C/script\u003E"); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.config.set({"wgBackendResponseTime":131,"wgHostname":"mw1045"}); } );</script>
	</body>
</html>
