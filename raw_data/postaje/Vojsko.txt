<!DOCTYPE html>
<html lang="sl" dir="ltr" class="client-nojs">
<head>
<meta charset="UTF-8" />
<title>Vojsko - Wikipedija, prosta enciklopedija</title>
<meta name="generator" content="MediaWiki 1.25wmf22" />
<link rel="alternate" href="android-app://org.wikipedia/http/sl.m.wikipedia.org/wiki/Vojsko" />
<link rel="alternate" type="application/x-wiki" title="Uredi stran" href="/w/index.php?title=Vojsko&amp;action=edit" />
<link rel="edit" title="Uredi stran" href="/w/index.php?title=Vojsko&amp;action=edit" />
<link rel="apple-touch-icon" href="//bits.wikimedia.org/apple-touch/wikipedia.png" />
<link rel="shortcut icon" href="//bits.wikimedia.org/favicon/wikipedia.ico" />
<link rel="search" type="application/opensearchdescription+xml" href="/w/opensearch_desc.php" title="Wikipedija (sl)" />
<link rel="EditURI" type="application/rsd+xml" href="//sl.wikipedia.org/w/api.php?action=rsd" />
<link rel="alternate" hreflang="x-default" href="/wiki/Vojsko" />
<link rel="copyright" href="//creativecommons.org/licenses/by-sa/3.0/" />
<link rel="alternate" type="application/atom+xml" title="Atom-vir strani »Wikipedija«" href="/w/index.php?title=Posebno:ZadnjeSpremembe&amp;feed=atom" />
<link rel="canonical" href="http://sl.wikipedia.org/wiki/Vojsko" />
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Vojsko","wgTitle":"Vojsko","wgCurRevisionId":3888748,"wgRevisionId":3888748,"wgArticleId":144831,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Razločitev","Razločitev imen krajev","Naselja v Sloveniji"],"wgBreakFrames":false,"wgPageContentLanguage":"sl","wgPageContentModel":"wikitext","wgSeparatorTransformTable":[",\t.",".\t,"],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy full","wgMonthNames":["","januar","februar","marec","april","maj","junij","julij","avgust","september","oktober","november","december"],"wgMonthNamesShort":["","jan.","feb.","mar.","apr.","maj","jun.","jul.","avg.","sep.","okt.","nov.","dec."],"wgRelevantPageName":"Vojsko","wgRelevantArticleId":144831,"wgIsProbablyEditable":true,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgWikiEditorEnabledModules":{"toolbar":true,"dialogs":true,"hidesig":true,"preview":false,"publish":false},"wgMediaViewerOnClick":true,"wgMediaViewerEnabledByDefault":true,"wgVisualEditor":{"pageLanguageCode":"sl","pageLanguageDir":"ltr"},"wgPoweredByHHVM":true,"wgULSAcceptLanguageList":[],"wgULSCurrentAutonym":"slovenščina","wgBetaFeaturesFeatures":[],"wgCategoryTreePageCategoryOptions":"{\"mode\":0,\"hideprefix\":20,\"showcount\":true,\"namespaces\":false}","wgNoticeProject":"wikipedia","wgWikibaseItemId":"Q413935","wgSiteNoticeId":"2.0"}); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.implement("user.options",function($,jQuery){mw.user.options.set({"variant":"sl"});},{},{},{});mw.loader.implement("user.tokens",function($,jQuery){mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\"});},{},{},{});
/* cache key: slwiki:resourceloader:filter:minify-js:7:8cfd4e3db0e866eea1792f07f45e244b */ } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.load(["mediawiki.page.startup","mediawiki.legacy.wikibits","mediawiki.legacy.ajax","ext.centralauth.centralautologin","mmv.head","ext.imageMetrics.head","ext.visualEditor.viewPageTarget.init","ext.uls.init","ext.uls.interface","ext.centralNotice.bannerController","skins.vector.js","ext.dismissableSiteNotice"]); } );</script>
<link rel="stylesheet" href="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=ext.uls.nojs%7Cext.visualEditor.viewPageTarget.noscript%7Cext.wikihiero%2CwikimediaBadges%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.sectionAnchor%7Cmediawiki.skinning.interface%7Cmediawiki.ui.button%7Cskins.vector.styles%7Cwikibase.client.init&amp;only=styles&amp;skin=vector&amp;*" />
<meta name="ResourceLoaderDynamicStyles" content="" />
<link rel="stylesheet" href="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=site&amp;only=styles&amp;skin=vector&amp;*" />
<style>a:lang(ar),a:lang(kk-arab),a:lang(mzn),a:lang(ps),a:lang(ur){text-decoration:none}
/* cache key: slwiki:resourceloader:filter:minify-css:7:c6f8341d5f258571daa89c807f484bf1 */</style>
<script src="//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false&amp;lang=sl&amp;modules=startup&amp;only=scripts&amp;skin=vector&amp;*"></script>
<link rel="dns-prefetch" href="//meta.wikimedia.org" />
<!--[if lt IE 7]><style type="text/css">body{behavior:url("/w/static-1.25wmf22/skins/Vector/csshover.min.htc")}</style><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr ns-0 ns-subject page-Vojsko skin-vector action-view">
		<div id="mw-page-base" class="noprint"></div>
		<div id="mw-head-base" class="noprint"></div>
		<div id="content" class="mw-body" role="main">
			<a id="top"></a>

							<div id="siteNotice"><!-- CentralNotice --><script>document.write("\u003Cdiv class=\"mw-dismissable-notice\"\u003E\u003Cdiv class=\"mw-dismissable-notice-close\"\u003E[\u003Ca href=\"#\"\u003Eskrij\u003C/a\u003E]\u003C/div\u003E\u003Cdiv class=\"mw-dismissable-notice-body\"\u003E\u003Cdiv id=\"localNotice\" lang=\"sl\" dir=\"ltr\"\u003E\u003Ctable class=\"plainlinks\" style=\"margin: 4px 10%; border-collapse: collapse; background: #f9f9f9; border: 1px solid #f4c430;\"\u003E\n\u003Ctr\u003E\n\u003Ctd style=\"border: none; padding: 2px 0px 2px 0.9em; text-align: center;\"\u003E\n  \u003Cdiv class=\"floatright\"\u003E\u003Ca href=\"/wiki/Slika:CEE_Spring_banner-600.xcf\" class=\"image\"\u003E\u003Cimg alt=\"CEE Spring banner-600.xcf\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/120px-CEE_Spring_banner-600.xcf.png\" width=\"120\" height=\"32\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/180px-CEE_Spring_banner-600.xcf.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/4/45/CEE_Spring_banner-600.xcf/240px-CEE_Spring_banner-600.xcf.png 2x\" data-file-width=\"600\" data-file-height=\"160\" /\u003E\u003C/a\u003E\u003C/div\u003E\u003C/td\u003E\n\u003Ctd style=\"border: none; padding: 0.25em 0.9em; width: 100%;\"\u003E V teku je spomladanska akcija pisanja člankov o \u003Ca href=\"/wiki/Srednja_Evropa\" title=\"Srednja Evropa\"\u003Esrednje-\u003C/a\u003E in \u003Ca href=\"/wiki/Vzhodna_Evropa\" title=\"Vzhodna Evropa\"\u003Evzhoevropskih\u003C/a\u003E državah - \u003Cb\u003E\u003Ca href=\"/wiki/Wikipedija:Wikimedia_CEE_Pomlad_2015\" title=\"Wikipedija:Wikimedia CEE Pomlad 2015\"\u003EWikimedia CEE Pomlad 2015\u003C/a\u003E\u003C/b\u003E. Vabljeni k vpisu in sodelovanju!\u003Cbr/\u003E\nDržavi za ta teden: \u003Ca href=\"/wiki/Slika:Flag_of_Azerbaijan.svg\" class=\"image\" title=\"Zastava Azerbajdžana\"\u003E\u003Cimg alt=\"Zastava Azerbajdžana\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/22px-Flag_of_Azerbaijan.svg.png\" width=\"22\" height=\"11\" class=\"thumbborder\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/33px-Flag_of_Azerbaijan.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Flag_of_Azerbaijan.svg/44px-Flag_of_Azerbaijan.svg.png 2x\" data-file-width=\"1200\" data-file-height=\"600\" /\u003E\u003C/a\u003E\u0026#160;\u003Ca href=\"/wiki/Azerbajd%C5%BEan\" title=\"Azerbajdžan\"\u003EAzerbajdžan\u003C/a\u003E (\u003Ca href=\"//meta.wikimedia.org/wiki/Wikimedia_CEE_Spring_2015/Structure/Azerbaijan\" class=\"extiw\" title=\"meta:Wikimedia CEE Spring 2015/Structure/Azerbaijan\"\u003Eideje\u003C/a\u003E) in \u003Ca href=\"/wiki/Slika:Flag_of_Belarus.svg\" class=\"image\" title=\"Zastava Belorusije\"\u003E\u003Cimg alt=\"Zastava Belorusije\" src=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/22px-Flag_of_Belarus.svg.png\" width=\"22\" height=\"11\" class=\"thumbborder\" srcset=\"//upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/33px-Flag_of_Belarus.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/8/85/Flag_of_Belarus.svg/44px-Flag_of_Belarus.svg.png 2x\" data-file-width=\"900\" data-file-height=\"450\" /\u003E\u003C/a\u003E\u0026#160;\u003Ca href=\"/wiki/Belorusija\" title=\"Belorusija\"\u003EBelorusija\u003C/a\u003E (\u003Ca href=\"//meta.wikimedia.org/wiki/Wikimedia_CEE_Spring_2015/Structure/Belarus\" class=\"extiw\" title=\"meta:Wikimedia CEE Spring 2015/Structure/Belarus\"\u003Eideje\u003C/a\u003E) \u003C/td\u003E\n\n\u003C/tr\u003E\n\u003C/table\u003E\n\u003C/div\u003E\u003C/div\u003E\u003C/div\u003E");</script></div>
						<div class="mw-indicators">
</div>
			<h1 id="firstHeading" class="firstHeading" lang="sl">Vojsko</h1>
						<div id="bodyContent" class="mw-body-content">
									<div id="siteSub">Iz Wikipedije, proste enciklopedije</div>
								<div id="contentSub"></div>
												<div id="jump-to-nav" class="mw-jump">
					Skoči na:					<a href="#mw-head">navigacija</a>, 					<a href="#p-search">iskanje</a>
				</div>
				<div id="mw-content-text" lang="sl" dir="ltr" class="mw-content-ltr"><p><b>Vojsko</b> je <a href="/wiki/Ime" title="Ime">ime</a> več <a href="/wiki/Naselje" title="Naselje">naselij</a> v Sloveniji:</p>
<ul>
<li><a href="/wiki/Vojsko,_Idrija" title="Vojsko, Idrija">Vojsko</a>, občina Idrija</li>
<li><a href="/wiki/Vojsko,_Kozje" title="Vojsko, Kozje">Vojsko</a>, občina Kozje</li>
<li><a href="/wiki/Vojsko,_Vodice" title="Vojsko, Vodice">Vojsko</a>, občina Vodice</li>
</ul>
<table id="disambigbox" class="metadata plainlinks dmbox dmbox-disambig" style="" role="presentation">
<tr>
<td class="mbox-image" style="padding: 2px 0 2px 0.4em;"><a href="/wiki/Slika:Disambig_gray.svg" class="image"><img alt="Ikona za razločitev" src="//upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Disambig_gray.svg/30px-Disambig_gray.svg.png" width="30" height="23" srcset="//upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Disambig_gray.svg/45px-Disambig_gray.svg.png 1.5x, //upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Disambig_gray.svg/60px-Disambig_gray.svg.png 2x" data-file-width="220" data-file-height="168" /></a></td>
<td class="mbox-text" style="padding: 0.25em 0.4em; font-style: italic;">Na tej <b>razločitveni strani</b> so našteti članki o različnih zemljepisnih legah z istim imenom.<br />
<small>Če vas je sem pripeljala <a class="external text" href="//sl.wikipedia.org/w/index.php?title=Posebno:WhatLinksHere/Vojsko&amp;namespace=0">notranja povezava</a>, jo lahko popravite tako, da bo usmerjena na pomensko ustrezno stran.</small></td>
</tr>
</table>


<!-- 
NewPP limit report
Parsed by mw1040
CPU time usage: 0.084 seconds
Real time usage: 0.121 seconds
Preprocessor visited node count: 253/1000000
Preprocessor generated node count: 0/1500000
Post‐expand include size: 2218/2097152 bytes
Template argument size: 535/2097152 bytes
Highest expansion depth: 18/40
Expensive parser function count: 0/500
-->

<!-- 
Transclusion expansion time report (%,ms,calls,template)
100.00%   44.739      1 - Predloga:Georaz
100.00%   44.739      1 - -total
 85.00%   38.026      1 - Predloga:Dmbox
 68.21%   30.518      1 - Predloga:Upravitelj_kategorij
 24.02%   10.745      1 - Predloga:Upravitelj_kategorij/blacklist
 21.72%    9.715      1 - Predloga:Upravitelj_kategorij/numbered
 16.30%    7.291      1 - Predloga:If_pagename
 13.66%    6.110      1 - Predloga:Namespace_detect
  7.77%    3.477      1 - Predloga:Main_other
-->

<!-- Saved in parser cache with key slwiki:pcache:idhash:144831-0!*!0!*!*!4!* and timestamp 20150326131247 and revision id 3888748
 -->
<noscript><img src="//sl.wikipedia.org/wiki/Special:CentralAutoLogin/start?type=1x1" alt="" title="" width="1" height="1" style="border: none; position: absolute;" /></noscript></div>									<div class="printfooter">
						Vzpostavljeno iz&#160;»<a dir="ltr" href="http://sl.wikipedia.org/w/index.php?title=Vojsko&amp;oldid=3888748">http://sl.wikipedia.org/w/index.php?title=Vojsko&amp;oldid=3888748</a>«					</div>
													<div id='catlinks' class='catlinks'><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Posebno:Kategorije" title="Posebno:Kategorije">Kategorije</a>: <ul><li><a href="/wiki/Kategorija:Razlo%C4%8Ditev" title="Kategorija:Razločitev">Razločitev</a></li><li><a href="/wiki/Kategorija:Razlo%C4%8Ditev_imen_krajev" title="Kategorija:Razločitev imen krajev">Razločitev imen krajev</a></li><li><a href="/wiki/Kategorija:Naselja_v_Sloveniji" title="Kategorija:Naselja v Sloveniji">Naselja v Sloveniji</a></li></ul></div></div>												<div class="visualClear"></div>
							</div>
		</div>
		<div id="mw-navigation">
			<h2>Navigacijski meni</h2>

			<div id="mw-head">
									<div id="p-personal" role="navigation" class="" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Osebna orodja</h3>
						<ul>
							<li id="pt-createaccount"><a href="/w/index.php?title=Posebno:Prijava&amp;returnto=Vojsko&amp;type=signup" title="Predlagamo vam, da ustvarite račun in se prijavite, vendar pa to ni obvezno.">Ustvari račun</a></li><li id="pt-login"><a href="/w/index.php?title=Posebno:Prijava&amp;returnto=Vojsko" title="Prijava ni obvezna, vendar je zaželena [o]" accesskey="o">Prijava</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Imenski prostori</h3>
						<ul>
															<li  id="ca-nstab-main" class="selected"><span><a href="/wiki/Vojsko"  title="Prikaže članek [c]" accesskey="c">Stran</a></span></li>
															<li  id="ca-talk" class="new"><span><a href="/w/index.php?title=Pogovor:Vojsko&amp;action=edit&amp;redlink=1"  title="Pogovor o strani [t]" accesskey="t">Pogovor</a></span></li>
													</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<h3 id="p-variants-label"><span>Različice</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Pogled</h3>
						<ul>
															<li id="ca-view" class="selected"><span><a href="/wiki/Vojsko" >Preberi</a></span></li>
															<li id="ca-ve-edit"><span><a href="/w/index.php?title=Vojsko&amp;veaction=edit"  title="Urejanje strani z VisualEditorjem [v]" accesskey="v">Uredi</a></span></li>
															<li id="ca-edit" class=" collapsible"><span><a href="/w/index.php?title=Vojsko&amp;action=edit"  title="Stran lahko uredite. Preden jo shranite, uporabite gumb za predogled. [e]" accesskey="e">Uredi kodo</a></span></li>
															<li id="ca-history" class="collapsible"><span><a href="/w/index.php?title=Vojsko&amp;action=history"  title="Prejšnje redakcije strani [h]" accesskey="h">Zgodovina</a></span></li>
													</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<h3 id="p-cactions-label"><span>Več</span><a href="#"></a></h3>

						<div class="menu">
							<ul>
															</ul>
						</div>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Iskanje</label>
						</h3>

						<form action="/w/index.php" id="searchform">
														<div id="simpleSearch">
															<input type="search" name="search" placeholder="Iskanje" title="Preiščite wiki [f]" accesskey="f" id="searchInput" /><input type="hidden" value="Posebno:Iskanje" name="title" /><input type="submit" name="fulltext" value="Iskanje" title="Najde vneseno besedilo po straneh" id="mw-searchButton" class="searchButton mw-fallbackSearchButton" /><input type="submit" name="go" value="Pojdi na" title="Pojdi na stran z natanko takim imenom, če obstaja" id="searchButton" class="searchButton" />								</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Glavna_stran"  title="Glavna stran"></a></div>
						<div class="portal" role="navigation" id='p-navigation' aria-labelledby='p-navigation-label'>
			<h3 id='p-navigation-label'>Navigacija</h3>

			<div class="body">
									<ul>
													<li id="n-mainpage"><a href="/wiki/Glavna_stran" title="Obiščite Glavno stran [z]" accesskey="z">Glavna stran</a></li>
													<li id="n-Dobrodo.C5.A1li"><a href="/wiki/Wikipedija:Uvod">Dobrodošli</a></li>
													<li id="n-Izbrani-.C4.8Dlanki"><a href="/wiki/Wikipedija:Izbrani_%C4%8Dlanki">Izbrani članki</a></li>
													<li id="n-randompage"><a href="/wiki/Posebno:Naklju%C4%8Dno" title="Naložite naključno stran [x]" accesskey="x">Naključni članek</a></li>
													<li id="n-recentchanges"><a href="/wiki/Posebno:ZadnjeSpremembe" title="Seznam zadnjih sprememb Wikipedije [r]" accesskey="r">Zadnje spremembe</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-obcestvo' aria-labelledby='p-obcestvo-label'>
			<h3 id='p-obcestvo-label'>Občestvo</h3>

			<div class="body">
									<ul>
													<li id="n-portal"><a href="/wiki/Wikipedija:Portal_ob%C4%8Destva" title="O projektu, kaj lahko storite, kje lahko kaj najdete">Portal občestva</a></li>
													<li id="n-Pod-lipo"><a href="/wiki/Wikipedija:Pod_lipo">Pod lipo</a></li>
													<li id="n-contact"><a href="/wiki/Wikipedija:Stik_z_nami">Kontaktna stran</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-podpora' aria-labelledby='p-podpora-label'>
			<h3 id='p-podpora-label'>Podpora</h3>

			<div class="body">
									<ul>
													<li id="n-help"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents" title="Kraj za pomoč">Pomoč</a></li>
													<li id="n-sitesupport"><a href="//donate.wikimedia.org/wiki/Special:FundraiserRedirector?utm_source=donate&amp;utm_medium=sidebar&amp;utm_campaign=C13_sl.wikipedia.org&amp;uselang=sl" title="Podprite nas">Denarni prispevki</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-coll-print_export' aria-labelledby='p-coll-print_export-label'>
			<h3 id='p-coll-print_export-label'>Tiskanje/izvoz</h3>

			<div class="body">
									<ul>
													<li id="coll-create_a_book"><a href="/w/index.php?title=Posebno:Book&amp;bookcmd=book_creator&amp;referer=Vojsko">Ustvari e-knjigo</a></li>
													<li id="coll-download-as-rdf2latex"><a href="/w/index.php?title=Posebno:Book&amp;bookcmd=render_article&amp;arttitle=Vojsko&amp;oldid=3888748&amp;writer=rdf2latex">Prenesi kot PDF</a></li>
													<li id="t-print"><a href="/w/index.php?title=Vojsko&amp;printable=yes" title="Natisljiva različica strani [p]" accesskey="p">Različica za tisk</a></li>
											</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-tb' aria-labelledby='p-tb-label'>
			<h3 id='p-tb-label'>Orodja</h3>

			<div class="body">
									<ul>
													<li id="t-whatlinkshere"><a href="/wiki/Posebno:KajSePovezujeSem/Vojsko" title="Seznam vseh s trenutno povezanih strani [j]" accesskey="j">Kaj se povezuje sem</a></li>
													<li id="t-recentchangeslinked"><a href="/wiki/Posebno:RecentChangesLinked/Vojsko" title="Zadnje spremembe na s trenutno povezanih straneh [k]" accesskey="k">Sorodne spremembe</a></li>
													<li id="t-specialpages"><a href="/wiki/Posebno:PosebneStrani" title="Preglejte seznam vseh posebnih strani [q]" accesskey="q">Posebne strani</a></li>
													<li id="t-permalink"><a href="/w/index.php?title=Vojsko&amp;oldid=3888748" title="Stalna povezava na to različico strani">Trajna povezava</a></li>
													<li id="t-info"><a href="/w/index.php?title=Vojsko&amp;action=info" title="Več informacij o strani">Podatki o strani</a></li>
													<li id="t-wikibase"><a href="//www.wikidata.org/wiki/Q413935" title="Povežite na ustrezni predmet v podatkovnem odložišču [g]" accesskey="g">Objekt Wikipodatki</a></li>
						<li id="t-cite"><a href="/w/index.php?title=Posebno:Navedi&amp;page=Vojsko&amp;id=3888748" title="Informacije o tem, kako navajati to stran">Navedba članka</a></li>					</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id='p-lang' aria-labelledby='p-lang-label'>
			<h3 id='p-lang-label'>V drugih jezikih</h3>

			<div class="body">
									<ul>
													<li class="interlanguage-link interwiki-en"><a href="//en.wikipedia.org/wiki/Vojsko" title="Vojsko – angleščina" lang="en" hreflang="en">English</a></li>
													<li class="interlanguage-link interwiki-hr"><a href="//hr.wikipedia.org/wiki/Vojsko" title="Vojsko – hrvaščina" lang="hr" hreflang="hr">Hrvatski</a></li>
													<li class="interlanguage-link interwiki-it"><a href="//it.wikipedia.org/wiki/Vojsko" title="Vojsko – italijanščina" lang="it" hreflang="it">Italiano</a></li>
													<li class="interlanguage-link interwiki-nl"><a href="//nl.wikipedia.org/wiki/Vojsko" title="Vojsko – nizozemščina" lang="nl" hreflang="nl">Nederlands</a></li>
													<li class="interlanguage-link interwiki-sr"><a href="//sr.wikipedia.org/wiki/%D0%92%D0%BE%D1%98%D1%81%D0%BA%D0%BE" title="Војско – srbščina" lang="sr" hreflang="sr">Српски / srpski</a></li>
													<li class="uls-p-lang-dummy"><a href="#"></a></li>
											</ul>
				<div class='after-portlet after-portlet-lang'><span class="wb-langlinks-edit wb-langlinks-link"><a href="//www.wikidata.org/wiki/Q413935#sitelinks-wikipedia" title="Uredi medjezikovne povezave" class="wbc-editpage">Uredi povezave</a></span></div>			</div>
		</div>
				</div>
		</div>
		<div id="footer" role="contentinfo">
							<ul id="footer-info">
											<li id="footer-info-lastmod"> Čas zadnje spremembe: 08:40, 10. marec 2013.</li>
											<li id="footer-info-copyright">Besedilo se sme prosto uporabljati v skladu z dovoljenjem <a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons 
Priznanje avtorstva-Deljenje pod enakimi pogoji 3.0</a>; uveljavljajo se lahko dodatni pogoji. Za podrobnosti glej <a href="//wikimediafoundation.org/wiki/Terms_of_Use">Pogoje uporabe</a>.<br/>
Wikipedia® je tržna znamka neprofitne organizacije <a href="http://wikimediafoundation.org">Wikimedia Foundation Inc.</a></li>
									</ul>
							<ul id="footer-places">
											<li id="footer-places-privacy"><a href="//wikimediafoundation.org/wiki/Politika_zasebnosti" title="wikimedia:Politika zasebnosti">Politika zasebnosti</a></li>
											<li id="footer-places-about"><a href="/wiki/Wikipedija:O_Wikipediji" title="Wikipedija:O Wikipediji">O Wikipediji</a></li>
											<li id="footer-places-disclaimer"><a href="/wiki/Wikipedija:Splo%C5%A1no_zanikanje_odgovornosti" title="Wikipedija:Splošno zanikanje odgovornosti">Zanikanja odgovornosti</a></li>
											<li id="footer-places-developers"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/How_to_contribute">Razvijalci</a></li>
											<li id="footer-places-mobileview"><a href="//sl.m.wikipedia.org/w/index.php?title=Vojsko&amp;mobileaction=toggle_view_mobile" class="noprint stopMobileRedirectToggle">Mobilni pogled</a></li>
									</ul>
										<ul id="footer-icons" class="noprint">
											<li id="footer-copyrightico">
															<a href="//wikimediafoundation.org/"><img src="//bits.wikimedia.org/images/wikimedia-button.png" srcset="//bits.wikimedia.org/images/wikimedia-button-1.5x.png 1.5x, //bits.wikimedia.org/images/wikimedia-button-2x.png 2x" width="88" height="31" alt="Wikimedia Foundation"/></a>
													</li>
											<li id="footer-poweredbyico">
															<a href="//www.mediawiki.org/"><img src="//bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="//bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_132x47.png 1.5x, //bits.wikimedia.org/static-1.25wmf22/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31" /></a>
													</li>
									</ul>
						<div style="clear:both"></div>
		</div>
		<script>/*<![CDATA[*/window.jQuery && jQuery.ready();/*]]>*/</script><script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.state({"ext.globalCssJs.site":"ready","ext.globalCssJs.user":"ready","site":"loading","user":"ready","user.groups":"ready"}); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.loader.load(["mediawiki.action.view.postEdit","mediawiki.user","mediawiki.hidpi","mediawiki.page.ready","mediawiki.searchSuggest","ext.wikiEditor.init","mmv.bootstrap.autostart","ext.imageMetrics.loader","ext.visualEditor.targetLoader","ext.eventLogging.subscriber","ext.wikimediaEvents.statsd","ext.navigationTiming","schema.UniversalLanguageSelector","ext.uls.eventlogger","ext.uls.interlanguage"],null,true); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { document.write("\u003Cscript src=\"//bits.wikimedia.org/sl.wikipedia.org/load.php?debug=false\u0026amp;lang=sl\u0026amp;modules=site\u0026amp;only=scripts\u0026amp;skin=vector\u0026amp;*\"\u003E\u003C/script\u003E"); } );</script>
<script>var _mwq = _mwq || []; _mwq.push( function ( mw ) { mw.config.set({"wgBackendResponseTime":110,"wgHostname":"mw1092"}); } );</script>
	</body>
</html>
