import re
import os.path

"""
Skripta iz prenešenih html datotek z Wikipedije pobere ustrezne podatke o postajah.
"""

postaje = os.listdir('postaje/')

# ker se imena postaj in Wiki strani ne ujemajo povsem niti strani niso povsem standardizirane, manjkajoče podatke vnesemo na roko

manjkajoči = {'SLOVENSKE KONJICE': 
                 {'regija': 'Savinjska regija', 'pokrajina':'Štajerska', 'občina': 'Občina Slovenske Konjice'}, 
              'LISCA' : 
                 {'regija': 'Spodnjeposavska regija','pokrajina':'Štajerska', 'občina':'Občina Sevnica', 'preb':'NULL', 'tip':'hrib'},
              'KRVAVEC':
                 {'regija': 'Gorenjska regija','pokrajina':'Gorenjska', 'občina':'Občina Cerklje na Gorenjekem', 'preb':'NULL', 'tip':'hrib'},
              'VOGEL' : 
                 {'regija': 'Gorenjska regija','pokrajina':'Gorenjska', 'občina':'Občina Bohinj', 'preb':'NULL', 'tip':'hrib'},
              'KREDARICA' : 
                 {'regija': 'Gorenjska regija','pokrajina':'Gorenjska', 'občina':'NULL', 'preb':'NULL', 'tip':'hrib'},
              'LJUBLJANA - BEZIGRAD' : 
                 {'regija': 'Osrednjeslovenska regija','pokrajina':'Gorenjska', 'občina':'Mestna občina Ljubljana'}}

def parsing(datoteka):
    """Funkcija pregleda html stran (ki je shranjena v datoteki) in iz nje pobere ustrezne podatke.
    Vrne tuple z najdenimi podatki. Če nekega podatka ne najde, na ustreznem mestu vrne None."""
    
    with open(datoteka, 'r', encoding='utf-8') as f:
        html = f.read()
        
    ime = datoteka[8:-4]
        
    # rezerva: vz_regija = re.compile(r'Statistična regija</a></b></td>\s*<td><a href="/wiki/[\w %-]+" title="([\w -]+)">\1</a></td>.*Tradicionalna pokrajina</a></b></td>\s*<td><a href="/wiki/[\w %-]+" title="([\w -]+)">\2</a></td>.*title="Občina">Občina</a></b></td>\s*<td><a href="/wiki/[\w %-]+" title="Občina ([\w -]+)">\3</a></td>.*<b>Skupno</b></td>\s*<td>([0-9.]+)</td>'
    
    # REGIJA
    vz_regija = re.compile(r'Statistična regija</a></b></td>\s*<td><a href="/wiki/[\w %-]+" title="([\w -]+)"')
    rezultat = re.findall(vz_regija, html)
    vz_regija2 = re.compile(r'Statistična regija</a></span></b></td>\s*<td><a href="/wiki/[\w %-]+" title="([\w -]+)"')
    rezultat2 = re.findall(vz_regija2, html)
    try:
        regija = rezultat[0]
    except Exception as e:
        try:
            regija = rezultat2[0]
        except Exception as e:
            regija = manjkajoči[ime]['regija']
            # regija = input('Ročno vnesi regijo za {}: '.format(datoteka[:-4]))
    
    # POKRAJINA
    vz_pokrajina = re.compile(r'Tradicionalna pokrajina</a></b></td>\s*<td><a href="/wiki/[\w \(\)%-]+" title="([\w -]+)( \(pokrajina\))?"')
    rezultat = re.findall(vz_pokrajina, html)
    vz_pokrajina2 = re.compile(r'Neformalna pokrajina</a></span></b></td>\s*<td><a href="/wiki/[\w \(\)%-]+" title="([\w -]+)( \(pokrajina\))?"')
    rezultat2 = re.findall(vz_pokrajina2, html)
    try:
        pokrajina = rezultat[0][0]
    except Exception as e:
        try:
            pokrajina = rezultat2[0][0]
        except Exception as e:
            pokrajina = manjkajoči[ime]['pokrajina']
            # pokrajina = input('Ročno vnesi pokrajino za {}: '.format(datoteka[:-4]))
    
    # OBČINA
    vz_obcina = re.compile(r'[Oo]bčina</a></b></td>\s*<td><a href="/wiki/[\w %-]+" title="([\w -]+)')
    rezultat = re.findall(vz_obcina, html)
    try:
        obcina = rezultat[0]
    except Exception as e:
        obcina = manjkajoči[ime]['občina']
        # obcina = input('Ročno vnesi občino za {}: '.format(datoteka[:-4]))
    
    # ŠTEVILO PREBIVALCEV
    vz_preb = re.compile(r'<td colspan="2"><b>Prebivalstvo</b>( \([0-9 .a-zčžš]+\)<sup id="[\w_ -]+" class="reference"><a href="[\w_ #-]+">\[[0-9]+\]</a></sup>)?</td>\s*</tr>\s*<tr class="mergedrow">\s*<td>&#160;•&#160;<b>Skupno</b></td>\s*<td>([0-9.]+)')
    rezultat = re.findall(vz_preb, html)
    try:
        preb = rezultat[0][1]
    except Exception as e:
        preb = manjkajoči[ime]['preb']
        # preb = input('Ročno vnesi število prebivalcev za {}: '.format(datoteka[:-4]))
    
    # TIP POSTAJE/KRAJA    
    vz_tip = re.compile(r'<p><b>[\w -]+</b>.*?<a href="/wiki/\w+" title="\w+">(\w+)</a>')
    rezultat = re.findall(vz_tip, html)
    try:
        tip = rezultat[0]
    except Exception as e:
        tip = manjkajoči[ime]['tip']
        # tip = input('Ročno vnesi tip za {}: '.format(datoteka[:-4]))
        
    return [ime, pokrajina, regija, obcina, preb, tip]
    
#parsing('postaje/VOJSKO.txt')
#parsing('postaje/BELI KRIZ.txt')
#parsing('postaje/BILJE.txt')
#parsing('postaje/JERUZALEM.txt')

with open('postaje_podatki.csv', 'w') as f:
    for datoteka in postaje:
        sez = parsing('postaje/' + datoteka)
        print(','.join(sez), file=f)
        
# na koncu v csv datoteki na roke popravimonekaj tipov, kjer je Wikipedia pač našla neustrezen tip (po naši klasifikaciji)
    