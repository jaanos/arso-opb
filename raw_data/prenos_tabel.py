import re
import urllib.request

"""
Skripta dobi imena postaj in sprinta vse linke, na katerih
se nahajajo tabele s podatki.
"""

url = 'http://meteo.arso.gov.si/met/sl/agromet/data/month/'
user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
headers = { 'User-Agent' : user_agent}

response = urllib.request.urlopen(url)
html = response.read().decode('utf-8')
html2 = re.sub(r'&Scaron;', 'Š', html) # ker so na strani tako zapisano Š-ji

with open('postaje.txt', 'w', encoding = 'utf-8') as f:
    f.write(html2)

# postaje shranim v slovar ((postaja brez šumnikov (le A-Z, _ in -), popolno ime postaje):(najmanjša, največja možna letnica))
postaje = {}

vzorec = re.compile(r'<option value="([A-Z_-]+)">([A-ZČĆŽŠĐ -]+) *\(([0-9]{4})-([ 0-9]+)\) </option>')
s = re.findall(vzorec, html2)

for i in s:
    postaje[(i[0].strip(), i[1].strip())] = (int(i[2]), int(i[3]) if i[3] != ' ' else 2015)

# seznam postaj napišem v -txt datoteko, da lahko dostopam do nje iz drugih skript
with open('postaje.txt', 'w', encoding='utf-8') as f:
    for pos in postaje:
        print(",".join(map(str, [pos[0], pos[1], postaje[pos][0], postaje[pos][1]])), file=f)


tabela_url_template = 'http://meteo.arso.gov.si/uploads/probase/www/agromet/product/form/sl/data/{0}_{1}{2:0>2}.txt'
months = list(range(1, 12))
for postaja_fn, _ in postaje:
    od, do = postaje[postaja_fn, _]
    for year in range(od, do+1):
        for month in months:
            print(tabela_url_template.format(postaja_fn, year, month))
