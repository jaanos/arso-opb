import psycopg2

"""
Skripta naredi osnovno strukuturo tabel v bazi. Če so tabele že narejene, ne naredi nič.
"""

CREATE_TABLES = [

# šifranti
"""
CREATE TABLE IF NOT EXISTS regija (
    id  serial PRIMARY KEY,
    ime varchar(80) NOT NULL
);
""",

"""
CREATE TABLE IF NOT EXISTS pokrajina (
    id  serial PRIMARY KEY,
    ime varchar(80) NOT NULL
);
""",

"""
CREATE TABLE IF NOT EXISTS tip (
    id   serial PRIMARY KEY,
    opis text NOT NULL
);
""",

# dejanski podatki
"""
CREATE TABLE IF NOT EXISTS postaje (
    id                 serial PRIMARY KEY,
    ime                varchar(80) NOT NULL,
    sirina             real NOT NULL,
    dolzina            real NOT NULL,
    visina             int NOT NULL,
    steviloPrebivalcev int,
    pokrajina          int REFERENCES pokrajina(id),
    regija             int REFERENCES regija(id),
    tip                int  REFERENCES tip(id)
);
""",

"""
CREATE TABLE IF NOT EXISTS meritve (
    id          serial PRIMARY KEY,
    postajaId   integer REFERENCES postaje(id) ON DELETE CASCADE,
    datum       date NOT NULL,
    constraint u_constraint UNIQUE (postajaId, datum)
);
""",


"""
CREATE TABLE IF NOT EXISTS tla (
    id          serial PRIMARY KEY,
    meritevId   int REFERENCES meritve(id),
    visina      int,
    temperatura real,
    constraint enolicnost UNIQUE (meritevId, visina, temperatura)
);
""",

"""
CREATE TABLE IF NOT EXISTS podatki (
    id              serial PRIMARY KEY,
    meritevId       int UNIQUE REFERENCES meritve(id),
    avgT            real,
    maxT            real,
    minT            real,
    minT5           real,
    avgW            real,
    avgC            int,
    avgRelH         int,
    avgP            int,
    rr              real,
    snegH           int,
    novSnegH        int,
    sonce           real,
    mocanVeter      boolean,
    viharniVeter    boolean,
    dez             boolean,
    rosenje         boolean,
    ploha           boolean,
    nevihta         boolean,
    grmenje         boolean,
    bliskanje       boolean,
    dezZ            boolean,
    rosenjeZ        boolean,
    ledeneIglice    boolean,
    sneg            boolean,
    zrnatSneg       boolean,
    plohaSneg       boolean,
    dezSneg         boolean,
    babjePseno      boolean,
    plohaDezSneg    boolean,
    toca            boolean,
    sodra           boolean,
    megla           boolean,
    meglaNebo       boolean,
    meglaLed        boolean,
    meglica         boolean,
    suhaMotnost     boolean,
    meglaNizka      boolean,
    rosa            boolean,
    slana           boolean,
    poledica        boolean,
    zled            boolean,
    ivje            boolean,
    ivjeTrdo        boolean,
    padavine        boolean,
    snegOdeja       boolean
);
""",

]

from auth import AUTH

try:
    conn = psycopg2.connect(**AUTH)
except psycopg2.OperationalError as e:
    print("I am unable to connect to the database with ATUH dict:", AUTH)
    print("Error:", e)

cursor = conn.cursor()
for create_table in CREATE_TABLES:
    cursor.execute(create_table)

conn.commit()
cursor.close()
conn.close()

print("Tabele so ustvarjene.")
