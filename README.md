# ARSO #

Projekt pri [OPB](https://ucilnica.fmf.uni-lj.si/course/view.php?id=257), matematika, 3. letnik.

## Viri podatkov ##

Glavni vir je spletna stran [Državne meteorološke službe](http://www.meteo.si/) Agencije Republike
Slovenije za okolje.

### ARSO ###
Na strani [ARSO](http://meteo.arso.gov.si/met/sl/agromet/data/) lahko dostopamo do dnevnih
vrednosti nekaterih agrometeoroloških podatkov:

- potencialna evapotranspiracija oz. izhlapevanje (etp),
- količina padavin (rr = rainfall rate),
- minimalna dnevna temperatura zraka (tmin),
- maksimalna dnevna temperatura zraka (tmax),
- povprečna dnevna temperatura zraka (tpov),
- minimalna dnevna temperatura zraka na višini 5 cm (tmin5),
- temeratura tal na različnih višinah.

Nekateri podatki so na voljjo v `.zip` datotekah, druge dobimo prek vmesnika. Lahko dostopamo tudi
direktno do `.txt` datotek, ki nam jih prikazuje vmesnik z linki oblike:
`http://meteo.arso.gov.si/uploads/probase/www/agromet/product/form/sl/data/{0}_{1}{2}.txt`

- 0 = ime postaje z velikimi tiskanimi črkami, tako kot je navedena na začetni strani med možnimi izbirami
- 1 = letnica, 1961 - 2015 (a pri nekaterih postajah manj!)
- 2 = mesec (vedno 2 mesti, torej npr. 04)

Primer:
[http://meteo.arso.gov.si/uploads/probase/www/agromet/product/form/sl/data/BIZELJSKO_201501.txt](http://meteo.arso.gov.si/uploads/probase/www/agromet/product/form/sl/data/BIZELJSKO_201501.txt)

Več podatkov je dostopnih skozi [grafični vmesnik](http://meteo.arso.gov.si/met/sl/app/webmet/).
Podatke smo kljub neprijaznosti vmesika programom uspeli dobiti, s precej primitivnimi metodami.
Lahko bi dobili tudi polurne podatke, a to zaenkrat ni potrebno, saj jih imamo dovolj.

## Podatki o bazi ##
Baza se nahaja na [baza.fmf.uni-lj.si](http://baza.fmf.uni-lj.si/).

### Avtentikacija ###
Za avtentikacijo pri programih moraš imeti v root folderju modul `auth.py`, ki iz varnostnih
razlogov ni vključen v repozitorij.
Sample file:
```python
"""
Authentication data to connect to the database.
"""

AUTH = {
    'host': 'baza.fmf.uni-lj.si',
    'user': '*username*',
    'password': '*password*',
    'port': 5432,
    'database': 'seminarska_jures',
}
```

### Shema baze ###
![Slika sheme](https://bitbucket.org/jureslak/arso-opb/raw/master/shema.png)

## Avtorji ##
Anja Petković, Vesna Iršič, Jure Slak
