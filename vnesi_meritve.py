import csv
import psycopg2

"""
Skripta v bazo vnese podatke o meritvah.
"""

print("Connecting...")

# povežem se z bazo
from auth import AUTH

try:
    conn = psycopg2.connect(**AUTH)
except psycopg2.OperationalError as e:
    print("\nI am unable to connect to the database with ATUH dict:", AUTH)
    print("Error:", e)

cursor = conn.cursor()

print("Done!")
print("Berem obstojece meritve iz baze ...")

cursor.execute("SELECT postajaId, datum FROM meritve")
meritve = cursor.fetchall()
meritve = {(str(x[0]), str(x[1])) for x in meritve}

print("Berem vse meritve iz datotek ...")

cursor.execute("SELECT id, ime FROM postaje")

ime_to_id = {ime: uid for uid, ime in cursor.fetchall()}

from opis_podatkov import convert_ime_postaje
from io import StringIO
import os
import datetime
# preberemo vse meritve

missing = set()
path = 'raw_data/gui'
filenames = os.listdir(path)
num_of_files = len(filenames)
c = 0
execute_meritve = []
execute_podatki = []
for filename in filenames:
    print("\r {} / {}      {:.1f}%".format(c, num_of_files, c/num_of_files*100), end=' ')
    with open(os.path.join(path, filename), encoding='utf-8') as f:
        podatki = csv.reader(f)
        next(podatki)
        for row in podatki:
            if not row: continue
            try:  # v bazo damo samo podatke, za katere imamo podatke o postajah.
                ime_postaje = convert_ime_postaje(row[1])
                datum = row[2]
                if all(row[i] == "" for i in range(3, len(row))): continue  # ce je vrstica brez podatkov jo preskocimo
                for i in range(len(row)):
                    if row[i].strip() == "":
                        row[i] = '\\N'
                execute_meritve.append((str(ime_to_id[ime_postaje]), datum))
                execute_podatki.append((ime_to_id[ime_postaje], datum) + tuple(row[3:]))
            except KeyError as e:
                missing.add(row[1])
    c += 1
print("\r {} / {}      {:.1f}%".format(c, num_of_files, c/num_of_files*100))

execute_meritve = set(execute_meritve)
execute_meritve -= meritve # damo stran vse ki so ze v bazi

if execute_meritve: # ce jih kaj se ostane
    # trick for speed
    f = StringIO('\n'.join(['\t'.join(x) for x in execute_meritve]))
    print("Missing stations:", missing)

    print("Shranjujem nove meritve v bazo ...")
    cursor.copy_from(f, 'meritve', columns=('postajaId', 'datum'))
    conn.commit()
    print("Done!")
else:
    print("Nothing to be done.")

print("Osvezujem meritve ...")

cursor.execute("SELECT * FROM meritve")
meritve = cursor.fetchall()
meritve = {(x[1], str(x[2])) : x[0] for x in meritve}

print("Pripravljam podatke ...")
execute_podatki = [(meritve[x[:2]],) + x[2:] for x in execute_podatki]  # zamenjaj datum in postajo v id meritve

print("Dobivam obstojece podatke iz baze ...")
cols = ("meritevId", "avgT", "maxT", "minT", "minT5", "avgW", "avgC", "avgRelH", "avgP", "rr",
"snegH", "novSnegH", "sonce", "mocanVeter", "viharniVeter", "dez", "rosenje", "ploha", "nevihta",
"grmenje", "bliskanje", "dezZ", "rosenjeZ", "ledeneIglice", "sneg", "zrnatSneg", "plohaSneg",
"dezSneg", "babjePseno", "plohaDezSneg", "toca", "sodra", "megla", "meglaNebo", "meglaLed",
"meglica", "suhaMotnost", "meglaNizka", "rosa", "slana", "poledica", "zled", "ivje", "ivjeTrdo",
"padavine", "snegOdeja",)

cursor.execute("SELECT * FROM podatki")
conv = lambda x: '\\N' if x is None or x == '\\N' else float(x)
stari_podatki = set(tuple(map(conv, x[1:])) for x in cursor)

execute_podatki = {tuple(conv(x) for x in i) for i in execute_podatki} # da se vse odšteje
execute_podatki -= stari_podatki
execute_podatki = [tuple(str(y).rstrip('0').rstrip('.') for y in x) for x in execute_podatki]

if execute_podatki:
    print("Shranjujem nove podatke v bazo ({} novih) ...".format(len(execute_podatki)))
    f = StringIO('\n'.join(['\t'.join(x) for x in execute_podatki]))
    cursor.copy_from(f, 'podatki', columns=cols)
    conn.commit()
    print("Done.")
else:
    print("Nothing to be done.")

conn.commit()
cursor.close()
conn.close()
